module gitlab.com/go-micro-pract/vessel-service

go 1.12

require (
	github.com/golang/protobuf v1.3.1
	github.com/micro/go-micro v1.7.0
	github.com/micro/protobuf v0.0.0-20180321161605-ebd3be6d4fdb // indirect
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859
)
