FROM ubuntu:latest

RUN mkdir -p /app/vessel-consignment

# Set our workdir to our current service in the gopath
WORKDIR /app/vessel-consignment

# Copy the current code into our workdir
COPY service-vessel .

CMD ["./service-vessel"]